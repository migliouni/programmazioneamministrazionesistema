#-------------------------------------------------
#
# Project created by QtCreator 2018-01-30T11:39:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProgettoQT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    item.h

FORMS    += mainwindow.ui
