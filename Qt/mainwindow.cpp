#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <fstream>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    set();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::set(){

   std::ifstream is;                    //stream per la lettura file
   std::string fileName = "./data.txt";
   is.open(fileName);                   //apertura file
   std::string oldRegion;               //memorizza ultima inserita in comboBox
   std::string fine="<END>";            //caratteri terminatore file

   bool end = false;                   //per sapere quando file finisce
   if(is.is_open()){                   //se si aperto lo stream ciclo fini alla fine
      while(!end) {
        std::string regione;
        std::string range;
        std::string nM, nF;
        is >> regione;                   //estraggo la regione ma potrei essere alla fine
        if(regione.compare(fine)!=0){
            is >> range >> nF >> nM ;
            int numM = std::stoi(nM);   //converto da string a intero
            int numF = std::stoi(nF);
            int totale = numM + numF;   //calcoli per percentuale
            double percU, percD;
            percU = (100 * (double)numM/totale);
            percD = (100 * (double)numF/totale);

            //creo item(una riga della tabella)
            item t(QString::fromStdString(regione), QString::fromStdString(range),numM, percU, numF, percD);
            //inserisco nel mio vector
            items.push_back(t);

            //se la regione è diversa da quella precedente la inserisco nella comboBox
            if(oldRegion != regione){
                oldRegion = regione;
                QString inserire =QString::fromStdString(regione);
                ui ->cbRegioni->addItem(inserire);
                }
            }
            else
                end = true;     //fine del file
        }
    }

    else
        ui->lbRegione->setText("Errore apertura file!");

    is.close();     //chiussura stream

    //setto per defaut l ultima regione letta
    //riempe la tabella con dati della regione, grazie allo SLOT
    ui->cbRegioni->setCurrentText(QString::fromStdString(oldRegion));
}



void MainWindow::on_cbRegioni_currentIndexChanged(const QString &arg1)
{
    //imposta la label con il nome della regione selezionata
    ui->lbRegione->setText(arg1);

    //svuota la tabella dai vecchi dati
    ui->twValori->setRowCount(0);


    int i = 0;            //indice per i-esima riga tabella
    unsigned int c = 0;   //indice per c-esimo elemento vector
    bool finito = false;  //indica se ho finito di leggere i dati della regione arg1

    //ciclo finchè non arrivo all fine oppure finche sono finiti i dati della regione
    while(c < items.size() && !finito){
      if(QString::compare(items[c].regione, arg1) == 0){    //se la regione = arg1

         //inserisco riga nuov
         ui->twValori->insertRow(ui->twValori->rowCount());
         //inserico nella riga i, nella colonna 0 1 2 3 4 i relativi dati
         ui->twValori->setItem(i,0,new QTableWidgetItem(items[c].range));
         ui->twValori->setItem(i,1,new QTableWidgetItem(QString::number(items[c].nU)));
         // 'g' serve per limitare il numero di cifre significative a 4
         ui->twValori->setItem(i,2,new QTableWidgetItem(QString::number(items[c].pU,'g',4)));
         ui->twValori->setItem(i,3,new QTableWidgetItem(QString::number(items[c].nD)));
         ui->twValori->setItem(i,4,new QTableWidgetItem(QString::number(items[c].pD,'g', 4)));
         i++;
        }
      else
          if(i > 0)
              finito = true;
      c++;
     }
}
