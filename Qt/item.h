#ifndef ITEM_H
#define ITEM_H

//rappresenta una riga della tabella, la regione permette di stabilire
//a quale regione appartengo i dati
struct item {
    QString regione;
    QString range;      //range di età
    int nU;             //numero di uomini
    double pU;          //percentuale uomini
    int nD;             //numero donne
    double pD;          //percentuale donne
    item(){}

    //costruttore
    item(const QString &reg, const QString &range, int nU, double pU, int nD, double pD)
        :regione(reg), range(range), nU(nU), pU(pU), nD(nD),pD(pD){}
};

#endif // ITEM_H

