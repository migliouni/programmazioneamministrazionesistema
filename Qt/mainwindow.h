#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "item.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void set();


private slots:

    void on_cbRegioni_currentIndexChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    std::vector<item> items;

};

#endif // MAINWINDOW_H
