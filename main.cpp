#include "cbuffer.hpp"  //classe cbuffer tempata
#include "voce.h"       //uso voce per fare test
#include <cassert>      //per assert
#include <list>
#include <vector>


//serie di funtori(predicati) per testare la funzione evaluate_if
//vero se numero pari
/**
	Funtore per interi che stabilisce se l'intero passato è pari
	Il funtore ritorna true se a è un numero pari.
	@brief Funtore per interi che stabilisce se l'intero passato è pari
**/

struct even {
	bool operator()(const int a) const {
		return (a % 2 == 0);
	}
};

//vero se numero dispari
/**
	Funtore per interi che stabilisce se l'intero passato è dispari
	Il funtore ritorna true se a è un numero dispari.
	@brief Funtore per interi che stabilisce se l'intero passato è dispari
**/

struct odd {
	bool operator()(const int a) const {
		return (a % 2 != 0);
	}
};

//vero se lunghezza stringa pari
/**
	Funtore per stringhe che stabilisce se la stringa passato è di lunghezza pari
	Il funtore ritorna true se s è una stringa di lunghezza pari.
	@brief Funtore per stringhe che stabilisce se la stringa passata è di lunghezza pari
**/

struct string_even {
	bool operator()(const std::string s) const {
		return (s.length() % 2 == 0);
	}
};

//vero se lunghezza stringa dispari
/**
	Funtore per stringhe che stabilisce se la stringa passato è di lunghezza dispari
	Il funtore ritorna true se s è una stringa di lunghezza dispari.
	@brief Funtore per stringhe che stabilisce se la stringa passata è di lunghezza dispari
**/

struct string_odd {
	bool operator()(const std::string s) const {
		return (s.length() % 2 != 0);
	}
};

//vero se il numero delle vocali del cognome è pari
/**
	Funtore per voci che stabilisce se la voce passata ha un numero di vocali pari
	Il funtore ritorna true se la voce v passata ha un numero di vocali pari
	@brief Funtore per voci che stabilisce se la voce passata ha un numero di vocali pari
**/

struct voce_even {
	bool operator()(const voce v) const {
		std::string cognome = v.cognome;
		int size = cognome.length();
		int cont = 0;
		for(int i=0; i < size; i++){
			char c = std::toupper(cognome[i]);
			if (c=='A' || c=='E' || c=='I' || c=='O' || c=='U')
				cont ++;
		}
		return (cont % 2 == 0);
	}
};

//vero se il numero delle vocali del cognome è dispari
/**
	Funtore per voci che stabilisce se la voce passata ha un numero di vocali dispari
	Il funtore ritorna true se la voce v passata ha un numero di vocali dispari
	@brief Funtore per voci che stabilisce se la voce passata ha un numero di vocali dispari
**/

struct voce_odd {
	bool operator()(const voce v) const {
		std::string cognome = v.cognome;
		int size = cognome.length();
		int cont = 0;
		for(int i=0; i < size; i++){
			char c = std::toupper(cognome[i]);
			if (c=='A' || c=='E' || c=='I' || c=='O' || c=='U')
				cont ++;
		}
		return (cont % 2 != 0);
	}
};

void testEvalueIf(){
	std::cout<<"---- Test su evaluate_if su int, string, voci con relativi funtori ----" << std::endl;
	std::cout<<std::endl <<"Interi: " << std::endl;
	cbuffer<int> cb(5);
	cb.insert(1);
	cb.insert(2);
	cb.insert(3);
	cb.insert(4);
	cb.insert(5);
	std::cout <<"Cbuffer: " << cb;
	
	//Chiamata a evaluate_if passando cbuffer di interi e funtore che stabilisce se numero pari: 
	std::cout <<"[i-esimo] elemento e' pari? " << std::endl;
	even a;
	evaluate_if(cb, a);
	
	//Chiamata a evaluate_if passando cbuffer di interi e funtore che stabilisce se numero pari: 
	std::cout <<"[i-esimo] elemento e' dispari? " << std::endl;
	odd b;
	evaluate_if(cb, b);
	
	//test su stringhe
	std::cout<<std::endl <<"Stringhe: " << std::endl;
	cbuffer<std::string> cb2(5);
	cb2.insert("Pippo");
	cb2.insert("Pluto");
	cb2.insert("Paperino");
	std::cout <<"Cbuffer: " << cb2;
	
	//Chiamata a evaluate_if passando cbuffer di stringhe e funtore che stabilisce se lunghezza stringa è pari 
	std::cout <<"[i-esimo] stringa e' di lunghezza pari? " << std::endl;
	string_even s;
	evaluate_if(cb2, s);
	
	//Chiamata a evaluate_if passando cbuffer di stringhe e funtore che stabilisce se lunghezza stringa è dispari
	std::cout <<"[i-esimo] stringa e' di lunghezza dispari? " << std::endl;
	string_odd k;
	evaluate_if(cb2, k);
	
	//test voci
	std::cout<<std::endl <<"Voci: " << std::endl;
	cbuffer<voce> cb3(5);
	cb3.insert(voce("Rossi","Cip", "1234567"));
    cb3.insert(voce("Verdi","Ciop", "3540852"));
    cb3.insert(voce("Gialli", "Bambi", "1110555"));
	std::cout <<"Cbuffer: " << cb3;
	
	//Chiamata a evaluate_if passando cbuffer di voci e funtore che stabilisce se il cognome ha vocali pari 
	std::cout <<"[i-esimo] voce e' ha numero vocali cognome pari? " << std::endl;
	voce_even even;
	evaluate_if(cb3, even);
	
	//Chiamata a evaluate_if passando cbuffer di voci e funtore che stabilisce se il cognome ha vocali dispari 
	std::cout <<"[i-esimo] voce e' ha numero vocali cognome dispari " << std::endl;
	voce_odd odd;
	evaluate_if(cb3, odd);
    std::cout << std::endl << std::endl;
}

void test_uso_stringhe(){
	std::cout<<"-------- Test su cbuffer di stringhe --------"<<std::endl;
	//inserimenti e rimozioni per testare il cbuffer con le strighe
	cbuffer<std::string> cb(4);
	assert(cb.isEmpty() == true);
	cb.insert("Pippo");
	cb.insert("Pluto");
	cb.insert("Paperino");
	
	assert(cb.inUse() == 3);
	assert(cb.size() == 4);
	assert(cb.isEmpty() == false);
	assert(cb.isFull() == false);
	
	//stampa il cbuffer 
	std::cout<<"Prima: " << cb;
	//elimina Pippo
	cb.remove();
	//stampa il cbuffer modificato
	std::cout<<"Dopo: " << cb;
	
	//inserisce altri elementi, l'elemento qua sosvrasscriverà Pluto
	cb.insert("Qui");
	cb.insert("Quo");
	assert(cb.isFull() == true);
	cb.insert("Qua");
	
	//stampa cbuffer modificato
	std::cout<<"Dopo: " << cb;
	std::cout<< std::endl;
	
	//test costruttore secondario
	cbuffer<std::string> cb2(4, "Topolino");
	std::cout<<cb2;
	
	//test costruttore per copia
	cbuffer<std::string> cb3(cb);
	std::cout << cb3;
	
	//test operatore uguale
	cbuffer<std::string> cb4;
	cb4 = cb2;
	std::cout << cb4 << std::endl;
	
	//test costruttore con due iteratori
	std::string a[5]= {"Minni", "Clarabella", "Paperone", "Silvestro", "Tom"};
	cbuffer<std::string> cb5(5, a, a+5);
	std::cout <<"Dopo uso di costruttore con iteratori: "<< std::endl << cb5<<std::endl;
	
	//test costruttore con due iteratori
	std::list <std::string> l;
	l.push_back("Prima");
	l.push_back("Seconda");
	l.push_back("Terza");
	std::list<std::string>::iterator inizio, fine;
	inizio = l.begin();
	fine = l.end();
	//passo dimensione più piccola dell dimensione reale, copierà solo i primi due
	cbuffer<std::string> cb6(2, inizio, fine);
	std::cout <<"Dopo uso di costruttore con iteratori di un vector: " << cb6<<std::endl;
	
	std::cout <<"Stampa con iteratori solo lettura: " << std::endl;
    cbuffer<std::string>::const_iterator i, ie;
    for(i = cb5.begin(), ie = cb5.end(); i!=ie; ++i)
	  std::cout<<*i <<" ";
	std::cout << std::endl << std::endl;
	
	cb5.setEmpty();
	assert(cb5.isEmpty() == true);
	
}

void test_uso_interi(){
  std::cout<<"-------- Test su cbuffer di interi --------" <<std::endl;
  cbuffer<int> cb(3);
  assert(cb.isEmpty() == true);
  //riempio cbuffer
  cb.insert(1);
  cb.insert(2);
  cb.insert(3);
  assert(cb.isFull() == true);
  
  std::cout<<"Cbuffer cb " << cb;
  cb.insert(888);
  std::cout<<"Cbuffer dopo insert 888: " << cb;
  cb.remove();
  std::cout<<"Cbuffer dopo remove: " << cb;
  
  //costruttore per copia
  cbuffer<int> cb2(cb);
  std::cout<<"Cbuffer cb2 dopo costruttore per copia " << cb2;
  
  //accedo ai primi due elementi test operatore []
  std::cout<<"Accedo con operatore[] agli elementi di posizione 0 1 2: " << std::endl;
  std::cout << cb2[0] <<" ";
  std::cout << cb2[1] <<" ";
  
  try {
	//provo ad accedere ad elemento fuori dal range  
	std::cout << cb2[2] <<" ";
  }
  catch(std::out_of_range e) {
	  std::cout << e.what() << std::endl;
  }
  
  //modifiche con operatore [] non cambiano anzianità
  std::cout<<"Modifiche con operatore []: " << std::endl;
  cb2[0] = 1000;
  std::cout<< cb2;
  assert(cb2.isFull() == false);
  assert(cb2.inUse() == 2);
  assert(cb2.size() == 3);
  
  cb2.setEmpty();
  assert(cb2.isEmpty() == true);
  
  //test operatore = con cb
  cbuffer<int> cb3;
  cb3 = cb;
  std::cout <<"Cbuffer cb3 dopo operatore = : " << cb3;
  
  cb3.clear();
  std::cout <<"Situazione dopo clear: " << "dimensione: " << cb3.size() << std::endl;
  std::cout <<"Cerco di accedere ad un cbuffer vuoto con operatore []: "<< std::endl;
  try{
	std::cout << cb3[0];	     //provo ad accedere
  } catch(std::out_of_range e){
	  std::cout << e.what()<<std::endl;     //intercetto eccezione e stampo messaggio
  }
  
  //test costruttore con due iteratori(puntatori di inizio e fine di un array)
  int a[5]= {1, 2, 3, 4, 5};
  cbuffer<int> cb4(5, a, a+5);
  std::cout <<"Dopo uso di costruttore con iteratori(puntatori): " << cb4;
  
  //test
  std::cout <<"Cerco di accedere ad un cbuffer oltre il limite con operatore []: "<< std::endl;
  try{										//uso scorretto
	std::cout << cb4[10]<<std::endl;		//accesso oltre il range consentito
  } 
  catch(std::out_of_range e){
	  std::cout<<e.what()<<std::endl;		//stampa messaggio
  }
  
  //test costruttore con due iteratori
  std::list<int> l;
  l.push_back(101);
  l.push_back(202);
  l.push_back(303);
  std::list<int>::iterator inizio, fine;
  inizio = l.begin();
  fine = l.end();
  //passo dimensione più grande, lo crea di 5 elementi e inserisce tutti quelli della lista
  cbuffer<int> cb5(5, inizio, fine);
   std::cout <<"Dopo uso di costruttore con iteratori di una list: " << cb5;
  
   std::cout <<"Stampa con iteratori solo lettura: " ;
   cbuffer<int>::const_iterator i, ie;
   for(i = cb4.begin(), ie = cb4.end(); i!=ie; ++i)
	  std::cout<<*i <<" ";
   std::cout << std::endl;
  
  cbuffer<int> cb6(0); 
  try{							//uso scorretto della classe
	cb6.insert(1);				//insert su cbuffer con dimensione 0, 
  }
  catch(std::out_of_range e){
	  std::cout<<e.what()<<std::endl;		//stampa messaggio
  }
  std::cout <<"Stampa cbuffer di dimensione 0: " << cb6;
  std::cout <<"size: "<<cb6.size()<<" in uso: " <<cb6.inUse();
  std::cout << std::endl << std::endl;
}

void test_uso_voce() {
  std::cout<<"-------- Test su cbuffer di voci --------" <<std::endl;
  cbuffer<voce> cb(3);
  //riempio cbuffer
  cb.insert(voce("Rossi","Cip", "1234567"));
  cb.insert(voce("Verdi","Ciop", "3540852"));
  cb.insert(voce("Gialli", "Bambi", "1110555"));
  std::cout<<"Cbuffer cb dopo inserimento di 3 voci: "<< std::endl << cb<< std::endl;

  assert(cb.isFull() == true);
  
  voce v1("Nano", "Brontolo", "7654321");
  cb.insert(v1);
  std::cout<<"Cbuffer cb dopo sovrascrittura: "<< std::endl << cb << std::endl;
  cb.remove();
  std::cout<<"Cbuffer dopo remove: " << std::endl <<cb<< std::endl;
  
  //costruttore per copia
  cbuffer<voce> cb2(cb);
  std::cout<<"Cbuffer cb2 dopo costruttore per copia " << std::endl << cb2 << std::endl;
  
  //accedo ai primi due elementi test operatore []
  std::cout<<"Accedo con operatore[] agli elementi di posizione 0 1 2: " << std::endl;
  std::cout << cb2[0] <<" ";
  std::cout << cb2[1] <<" ";
  
  try {
	//provo ad accedere ad elemento fuori dal range  
	std::cout << cb2[2] <<" ";
  }
  catch(std::out_of_range e) {
	  std::cout << e.what() << std::endl<< std::endl;
  }
  
  //modifiche con operatore [] non cambiano anzianità
  std::cout<<"Modifiche con operatore []: " << std::endl;
  cb2[0] = voce("Ferrari", "Geppetto", "2342224");
  std::cout<< cb2 << std::endl;
  assert(cb2.isFull() == false);
  assert(cb2.inUse() == 2);
  assert(cb2.size() == 3);
  cb2.setEmpty();
  assert(cb2.isEmpty() == true);
  
  //test operatore = con cb
  cbuffer<voce> cb3;
  cb3 = cb;
  std::cout <<"Cbuffer cb3 dopo operatore = : " << cb3 << std::endl;
  
  cb3.clear();
  std::cout <<"Situazione dopo clear: " << "dimensione: " << cb3.size() << std::endl << std::endl;
  
  //test costruttore con due iteratori
  voce a[3] = {voce("Russo", "Ugo", "1523579"), voce("Gallo", "Luca", "9922551"), voce("Ricci", "Andrea", "2342224")};
  cbuffer<voce> cb4(3, a, a+3);
  std::cout <<"Dopo uso di costruttore con iteratori: " << std::endl<<cb4<< std::endl;
  
  //test costruttore con due iteratori
  std::vector <voce> v;
  v.push_back(voce("Russo", "Ugo", "1523579"));
  v.push_back(voce("Russo", "Luigi", "3542179"));
  v.push_back(voce("Russo", "Marco", "1529555"));
  std::vector<voce>::iterator inizio, fine;
  inizio = v.begin();
  fine = v.end();
  //passo dimensiosione esatta
  cbuffer<voce> cb5(3, inizio, fine);
  std::cout <<"Dopo uso di costruttore con iteratori di un vector: "<< std::endl << cb5 << std::endl;
  
  std::cout <<"Stampa con iteratori solo lettura: " << std::endl;
  cbuffer<voce>::const_iterator i, ie;
  for(i = cb4.begin(), ie = cb4.end(); i!=ie; ++i)
	std::cout<<*i << " ";
  
  std::cout << std::endl << std::endl;
  
}

void test_cbuffer_const(){
  std::cout<<"-------- Test su const cbuffer --------" <<std::endl;
  cbuffer<int> cb(4);
  //riempio cbuffer
  cb.insert(1);
  cb.insert(2);
  cb.insert(3);
 
  //uso costruttore per copia
  const cbuffer<int> cb_const(cb);
  std::cout << "Cbuffer costante: " << cb_const<< std::endl;
  
  //Metodi non const non richiamabili
  //cb_const.insert(4); //errore 
  //cb_const.remove();  //errore
  //cb_const.clear();   //errore
  
  //costruttore per copia
  assert(cb.isFull() == false);
  assert(cb.isEmpty() == false);
  const cbuffer<int> cb_const2(cb_const);
  std::cout <<"Cbuffer cb_const2 dopo costruttore per copia " << cb_const2<< std::endl;
  std::cout <<"Dimensione: " << cb_const2.size() << " In uso: " << cb_const2.inUse() <<std::endl << std::endl;
  
  
  //accedo ai primi due elementi test operatore []
  std::cout<<"Accedo con operatore[] agli elementi di posizione 0 1 2 3: " << std::endl;
  std::cout << cb_const2[0] <<" ";
  std::cout << cb_const2[1] <<" ";
  std::cout << cb_const2[2] <<" ";
  
  try {
	//provo ad accedere ad elemento fuori dal range  
	std::cout << cb_const2[3] <<" ";
  }
  catch(std::out_of_range e) {
	  std::cout << e.what() << std::endl<< std::endl;
  }
  
  //Modifiche con operatore [] portano ad errore
  //cb_const2[0] = 1000;
  
  cbuffer<int> cb_const3;
  cb_const3 = cb_const;
  std::cout <<"Cbuffer cb_const3 dopo operatore = : " << cb_const3<< std::endl;
  
  std::cout <<"Stampa con iteratori solo lettura: " << std::endl;
  cbuffer<int>::const_iterator i, ie;
  for(i = cb_const3.begin(), ie = cb_const3.end(); i!=ie; ++i)
	 std::cout<<*i << " ";
  std::cout << std::endl << std::endl;
}

void test_iteratori() {
  std::cout<<"-------- Test su iteratori --------" <<std::endl;
  cbuffer<int> cb(3);
  //riempio cbuffer
  cb.insert(1);
  cb.insert(2);
  cb.insert(3);
 
  cbuffer<int>::iterator i, ie;
  i = cb.begin();	//punta all'elemento 1
  ie = cb.end(); 	//punta alla cella successiva all'ultimo elemento(dopo 3)
  for(; i!=ie; i++)
	  std::cout<< *i <<" "; //stampa 1 2 3
  std::cout<< std::endl;
  
  //std::cout<< *i;	//uso errato dell'iteratore stampa un valore a caso
  
  i--;
  std::cout<<"Dopo decremento: " << *i <<std::endl;	
  std::cout<<"Prova -offset: " << *(i-2) <<std::endl;
  i-=2;
  std::cout<<"Dopo i-=2 : " << *i <<std::endl;
  i+=2;
  std::cout<<"Dopo i-=2 : " << *i <<std::endl;
  std::cout<<"Prova differenza tra due iteratori: " << i-ie <<std::endl;
  assert(i < ie);
  assert(i <= ie);
  assert(ie > i);
  assert(ie >= i);
  assert((ie == i) == false);
  
  std::cout<<"Operatore [-1]: "<< i[-1] <<std::endl;
  std::cout<<"Operatore [0]: "<< i[0] <<std::endl;
  i[0] = 77;
  //std::cout<<i[1]; //uso scorretto vado fuori dal range
  --ie;
  std::cout<<"Pre-decremento ie: "<<*(ie--) << std::endl;
  std::cout<<"Post-decremento ie: "<<*(--ie) <<std::endl;
  std::cout<<"Pre-incremento ie: "<<*(ie++) << std::endl;
  std::cout<<"Post-incremento ie: "<<*(++ie) <<std::endl;
  
  std::cout << std::endl;
  
  //test su stringhe
  cbuffer<std::string> cb2(3);
  //riempio cbuffer
  cb2.insert("Pippo");
  cb2.insert("Pluto");
  cb2.insert("Paperino");
  
  cbuffer<std::string> ::const_iterator i2, ie2;
  i2 = cb2.begin();
  ie2 = cb2.end();
  for(; i2 != ie2; i2++)
	  std::cout<< *i2 <<" ";
  std::cout << std::endl;
  
  i2--;
  std::cout<<"Dopo decremento: " << *i2 <<std::endl;	
  std::cout<<"Prova -offset: " << *(i2-2) <<std::endl;
  i2-=2;
  std::cout<<"Dopo i2-=2 : " << *i2 <<std::endl;
  i2+=2;
  std::cout<<"Dopo i2-=2 : " << *i2 <<std::endl;
  std::cout<<"Prova differenza tra due iteratori: " << i2-ie2 <<std::endl;
  assert(i2 < ie2);
  assert(i2 <= ie2);
  assert(ie2 > i2);
  assert(ie2 >= i2);
  assert((ie2 == i2) == false);
  
  std::cout<<"Operatore [-1]: "<< i2[-1] <<std::endl;
  std::cout<<"Operatore [0]: "<< i2[0] <<std::endl;
  //std::cout<<i2[1]; //uso scorretto vado fuori dal range
  //i2[0] = "prova"; //errore const operazioni solo in lettura
  --ie2;
  std::cout<<"Pre-decremento ie2: "<<*(ie2--) << std::endl;
  std::cout<<"Post-decremento ie2: "<<*(--ie2) <<std::endl;
  std::cout<<"Pre-incremento ie2: "<<*(ie2++) << std::endl;
  std::cout<<"Post-incremento ie2: "<<*(++ie2) <<std::endl<< std::endl;
  
  
  //test su voci, operatori iterator < > = const_iterator
  //e viceversa
  cbuffer<voce> cbVoci(5);
  cbVoci.insert(voce("Russo", "Ugo", "1523579"));
  cbVoci.insert(voce("Gallo", "Luca", "9922551"));
  cbVoci.insert(voce("Ricci", "Andrea", "2342224"));
  cbuffer<voce>::iterator i3, ie3;
  i3 = cbVoci.begin();
  ie3 = cbVoci.end();
  for(; i3 != ie3; i3++)
	  std::cout<<*i3 <<" ";
   std::cout << std::endl;
   i3[-2] = voce("Gabrielli", "Filippo", "1357842");
  
  
  cbuffer<voce>::const_iterator i4, ie4;
  i4 = cbVoci.begin();
  ie4 = cbVoci.end();
  std::cout<<"Prova ->" << i4->cognome <<std::endl;
   for(; i4 != ie4; i4++)
	  std::cout<<*i4 <<" ";
  
  
  assert(i3 == i4);
  i3--;
  assert(i3 != ie4);
  
  assert(i3 < ie4);
  assert(ie3 >= ie4);
  assert(ie3 <= ie4);
  ie4--;
  assert(ie3 > ie4);
  
  ie4++;
  i3++;
  assert(i4 == i3);
  i4--;
  assert(i4 != ie3);
  i4++;
  ie3--;
  assert(i4 > ie3);
  ie3++;
  ie4 --;
  assert(ie4 <= ie3);
  --ie3;
  ++ie4;
  assert(ie4 >= ie3);
  
  ie4--;
  --ie4;
  ie3++;
  assert(ie4 < ie3);
  
  std::cout << std::endl;
  
}
int main(void) {
  std::cout << std::endl;
  test_uso_interi();
  test_uso_stringhe();
  test_uso_voce();
  test_cbuffer_const();
  testEvalueIf();
  test_iteratori();
  return 0;
}



