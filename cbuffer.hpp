#ifndef CBUFFER_HPP   //guardia per evitare inclusioni multiple
#define CBUFFER_HPP
#include <iostream>   //per cin cout
#include <algorithm>  //per swap
#include <stdexcept>  //per eccezzioni std::out_of_range
#include <iterator> // std::random_iterator_tag
#include <cstddef>  // std::ptrdiff_t
/**
@file cbuffer.hpp
@brief Dichiarazione e implementazione della classe cbuffer
**/
/**
@brief Array dinamico circolare di elementi templati
   
Classe che rappresenta un array dinamico templato e circolare di dimensine fissa scelta in fase di costruzione dell'oggetto.
**/
template <typename T>
class cbuffer {
public:
  typedef T value_type; 
  typedef unsigned int size_type;	///< Definizione del tipo corrispondente alla dimensione e per indici 

private:
  T * _buffer;	    ///< Puntatore ad elementi di tipo generico T
  size_type _fine;  ///< Indica l indice del prossimo elemento 
  size_type _inUso;	///< Indica il numero di elementi che sono stati inseriti
  size_type _dimensione; ///< Indica dimensione del cbuffer 

public:
  /**
  @brief Metodo che setta in un stato di vuoto logico il cbuffer, il cbuffer 
  è da considerare vuoto se gli elementi _inUso sono 0
  **/
  void setEmpty(){	
    _fine = 0;	
	_inUso = 0;
  }
  /**
  @brief Metodo che stabilisce se il cbuffer è pieno
  Ritorna true se il cbuffer è pieno, false altrimenti.
  Il cbuffer è pieno se il numero degli gli elementi realmente in uso(_inUso) è uguale alla dimensione
  @return Ritorna true se il cbuffer è pieno, false altrimenti
  **/
  
  bool isFull() const {						
    return (_inUso == _dimensione);
  }

  /**
  @brief Metodo che stabilisce se il cbuffer è vuoto
  Ritorna true se il cbuffer è vuot, false altrimenti, il buffer è vuoto se il numero degli gli elementi realmente in uso(_inUso) è uguale a 0
  @return Ritorna true se il cbuffer è vuoto, false altrimenti
  **/
  bool isEmpty() const {
    return (_inUso == 0);
  }

   /**
  @brief Metodo che dealloca la memoria allocata dal cbuferr
  Metodo sfruttato dal distruttore, setta lo stato logico di cbuffer vuoto
  **/
  void clear(){
    delete[] _buffer;
    _buffer = 0;
    _dimensione = 0;
    setEmpty();
  }

  /**
  @brief Numero di elementi realmente in Uso nel cbuffer
  Ritorna numero di elementi realmente in Uso nel cbuffer
  @return Numero di elementi realmente in Uso nel cbuffer
  **/
  size_type inUse() const{
    return _inUso;
  }
  
  /**
  @brief Dimensione del cbuffer
  Ritorna la dimensine del cbuffer 
  @return Ritorna la dimensine del cbuffer
  **/
  size_type size() const{
    return _dimensione;
  }

  /**
  @brief Costruttore di default (METODO FONDAMENTALE)
  Costruttore di default per instanziare un cbuffer vuoto, questo è
  l'unico costruttore che può essere usato per creare un array di cbuffer!
  **/
  
  //costruttore default vuoto
  //con initialization list setto il cbuffer nello stato di default
  cbuffer():  _fine(0), _buffer(0), _dimensione(0), _inUso(0) {
#ifndef NDEBUG
    std::cout << "cbuffer()" << std::endl;
#endif
  }
  
  /**
  @brief Costruttore per copia (METODO FONDAMENTALE)
  Costruttore di copia, permette di instanzaiare un cbuffer copiando i
  valori da un altro cbuffer.
  @param other cbuffer da usare per creare il nuovo cbuffer
  @throw eccezione di allocazione di memoria
  **/
    
  //costruttore per copia
  cbuffer(const cbuffer &other):  _fine(0), _inUso(0), _buffer(0), _dimensione(0) {
    _buffer = new T [other._dimensione];   //provo ad allocare memoria sufficiente
    _dimensione = other._dimensione;       //se non fallisce la new assegno la dimensione
	_fine = other._fine;                   //faccio copia dei dati membro della classe
	_inUso = other._inUso;
    
    //faccio try catch per catturare eccezioni durante il riempimento del cbuffer
    //nel caso di eccezioni catturarle e ripristinare uno stato dell'oggetto coerente
    try {   
      if(!this->isEmpty())  						//controllo se vuoto, in caso positivo ho finito
		for(size_type i = 0; i < _inUso; i++)  //posso copiare da 0 a quelli in uso				   
			_buffer[i] = other._buffer[i];  
    }
    catch(...){             //in caso di eccezzione devo ripristinare in stato coerente l'oggeto
      clear();				//sfrutto funzione clear
      throw;               //propago l'eccezione al chiamante in modo che sappia che c'è stata un'eccezione
    }
    
#ifndef NDEBUG
    std::cout << "cbuffer(const cbuffer &other)" << std::endl;
#endif
  } 
	/**
	@brief Operatore di assegnamento (METODO FONDAMENTALE)
	Operatore di assegnamento, permette la copia tra cbuffer, sfruttando il metodo swap
	@param other cbuffer sorgente
	@return Riferimento a this
	@throw eccezione di allocazione di memoria
	**/
	
  //operatore assegnamento
  cbuffer &operator=(const cbuffer &other) {
    if(this != &other) { //controllo autoassegnamento
      cbuffer tmp(other);
      this->swap(tmp);
    }
    
#ifndef NDEBUG
    std::cout << "cbuffer &operator=(const cbuffer &other)" << std::endl;
#endif
    return *this;	//ritorno puntatore a this
  }
	/**
	@brief Scambia il contenuto di due cbuffer
	Scambia il contenuto di due cbuffer.
	@param other cbuffer con il quale scambiare i dati 
	**/
	
  void swap(cbuffer &other) {
    std::swap(other._dimensione, this-> _dimensione); //scambio dati
    std::swap(other._buffer, this-> _buffer);
    std::swap(other._fine, this-> _fine); 
	std::swap(other._inUso, this-> _inUso); 
  }

    /**
	@brief Distruttore (METODO FONDAMENTALE)
	Distruttore, rimuove la memoria allocata da cbuffer.
	**/
	
  //distruttore
  ~cbuffer() {
   clear();
   
#ifndef NDEBUG
    std::cout << "~cbuffer()" << std::endl;
#endif

  }

	/**
    @brief Costruttore secondario
	Costruttore secondario. Permette di istanziare un cbuffer con una data dimensione 
	@param dim del dbuffer da istanziare
	@throw eccezione di allocazione di memoria
	**/
	
  //costruttore secondario con dimensione dim
  explicit cbuffer(size_type dim):  _fine(0), _inUso(0), _buffer(0), _dimensione(0) {
    _buffer = new T[dim];	//tenta di allocare memoria
    _dimensione = dim;  	//se riesce allocazione setta _dimensione
	_inUso = 0;
	#ifndef NDEBUG
		std::cout << "explicit cbuffer(size_type dim)" << std::endl;
	#endif
  }
  
	/**
	@brief Costruttore secondario
	Costruttore secondario. Permette di istanziare un cbuffer con una data dimensione e di inizializzare le celle dell'array con il valore passato
	@param dim del dbuffer da istanziare
	@param value Valore da usare per inizizalizzare le celle dell'array
	@throw eccezione di allocazione di memoria
	**/
	
  //costruttore secondario con dimensione dim e un valore di default
  cbuffer(size_type dim, const T &value):  _fine(0), _inUso(0), _buffer(0), _dimensione(0)
  {
    _buffer = new T[dim];		//tento di allocare
    _dimensione = dim;			
    try {	//provo a settare ogni cella al valore 
		for(size_type i=0; i < dim; i++)
			_buffer[i] = value;
	
	_fine = 0;	//buffer pieno
	_inUso = dim;
    }
    catch(...) {	//in caso di eccezione delloco e rilancio eccezione
		clear();	//sfrutto clear
		throw;
    }
      
#ifndef NDEBUG
      std::cout << "cbuffer(size_type dim, const T &value)" << std::endl;
#endif
    }

	/**
	@brief Inserisce elemento in coda
	Inserisce elemento in coda del cbuffer, in caso di cbuffer pieno sovrascrive l'elemento più anziano. 
	@param value valore da inserire del cbuffer
	@throw std::out_of_range se si prova a fare insert su cbuffer di dimensione 0
	**/
	
  //inserisce alla fine del cbuffer, 
  void insert(const T &value) {
	
	//se il buffer è di dimensione 0 non posso inserire alcun elemento 
	if(_dimensione == 0){
		throw std::out_of_range("Cbuffer di dimensione 0!");
	}
		
    //se il cbuffer è vuoto  posso inserire senza problemi
	if(this->isEmpty()) {
      _buffer[_fine] = value;		   //inserisco l elemento in coda(_fine)
	  _fine = (++_fine) % _dimensione; //aggiorno indice _fine e contatore
	  _inUso ++;
    }
    else{ //se il cbuffer è pieno devo inserire al posto del più anziano
      if(this->isFull()) {
		_buffer[_fine] = value; //inserisco in posizione _fine
		//devo riportarmi nella situazione di di array che va da 0 a _inUso
		/* salvo l elemento in posizione 0, dato che ho eliminato l'elemneto
		   più anziano, devo far scalare gli elementi di una posizione, perchè l ultimo inserito dovrà ritrovarsi in coda al cbuffer.
		*/
		T tmp =_buffer[0]; 		
		for(size_type i = 1; i <_dimensione; i++) 
             _buffer[i - 1] = _buffer[i];
		_buffer[_dimensione - 1] = tmp;
		_fine = 0;	//l'elemento più anziano sarà in posizione _fine	
      }
	  //se non è pieno c'è ancora spazio si può procedere senza problemi
      else { 
		_buffer[_fine] = value;
		_fine = (++_fine) % _dimensione;
		_inUso ++;
      } 
	}
    
	#ifndef NDEBUG
      std::cout << "void insert(const T &value)" << std::endl;
	  std::cout<<"Inizio: 0" << " Fine " << _fine << " Inserito " << value << std::endl;
	#endif 
  }
  
	/**
	@brief Elimina l'elemento in testa
	Elimina l'elemento in testa del cbuffer, se il cbuffer è vuoto non fa alcuna operazione
	**/
	
  //elimina l elemento in testa 
  void remove() {
    if(!this->isEmpty()){	//controllo che il buffer non sia vuoto
	
	#ifndef NDEBUG
      T rimosso = _buffer[0];
	#endif
	  //se c'è un solo elemento devo settare lo stato di vuoto
	  if(_inUso == 1)
		setEmpty();	
	  else { 
		/*la remove è solo logica, semplicemente gli elementi in uso  vengono decrementati e la nuova fine corrisponde a _inUso.
		Dato che l'elemento in posizione 0 è quello più anziano basta far scalare gli elementi _inUso del buffer in modo da mettere l' elemento più vecchio in posizione 0.
		*/
		 
		for(size_type i = 1; i <_inUso; i++)
           _buffer[i - 1] = _buffer[i];
		_inUso --; 	 //decremento contatori
		_fine --; 
	  } 
	
	#ifndef NDEBUG
	  std::cout<<"Inizio 0" << " Fine " << _fine << " Rimosso " << rimosso << std::endl;
	#endif	
	  
    } 
  }
  
    /**
	@brief Accesso ai dati in lettura/scrittura 
	Metodo per leggere/scrivere il valore dell index-esimo elemento
	@param index Indice della cella dell'array da leggere
	@return Il valore della cella index-esima
	@throw std::out_of_range se cbuffer vuoto 
	@throw std::out_of_range se cbuffer non pieno e index >= fine
	**/
	
  T &operator[](size_type index)
  {
	//se vuoto eccezione, nessun elemento è accessibile 
    if(this->isEmpty())
		throw std::out_of_range("Cbuffer vuoto!"); 
	//se è pieno ogni indice è accessibile
	//se non è pieno ma l'indice a cui voliamo accedere < fine
	//index sempre > 0 perchè index è di tipo size_type
	if(index < _inUso ) 
	   return _buffer[index];
	else
    throw std::out_of_range("Indice fuori dal range di elementi accessibili!");
	
	#ifndef NDEBUG
	  std::cout << "T &operator[](size_type index) " << index << std::endl; 
	#endif	
	
  }
      
	/**
	@brief Accesso ai dati in lettura 
	Metodo per leggere il valore dell index-esimo elemento
	@param index Indice della cella dell'array da leggere
	@return Il valore della cella index-esima
	@throw std::out_of_range se cbuffer vuoto 
	@throw std::out_of_range se cbuffer non pieno e index > _inUso
	**/
	
  const  T &operator[](size_type index) const {
   
    //se vuoto eccezione, nessun elemento è accessibile 
    if(this->isEmpty())
		throw std::out_of_range("Cbuffer vuoto!"); 
	//se è pieno ogni indice è accessibile
	//se non è pieno ma l'indice a cui voliamo accedere < fine
	//index sempre > 0 perchè index è di tipo size_type
	if(index < _inUso ) 
	   return _buffer[index];
	else
    throw std::out_of_range("Indice fuori dal range di elementi accessibili!");
	
	#ifndef NDEBUG
	  std::cout << "T &operator[](size_type index) " << index << std::endl; 
	#endif	
	
	}
       
    /**
	Costruttore secondario che costruisce il cbuffer a partire da una dimensione e due iteratori.
	@param size dimensione di memoria da allocare
	@param begin iteratore di inizio della sequenza
	@param end iteratore di fine della sequenza
	@throw eccezione di allocazione di memoria
	*/
	
  template <typename itQ>
  cbuffer(size_type size, itQ begin, itQ  end) : _fine(0), _inUso(0), _buffer(0), _dimensione(0)
  {
    _buffer = new T[size];   //provo ad allocare memoria
    _dimensione = size;      //se non fallisce l'allocazione assegno la dimensione del mio cbuffer

    try {
	  int inseriti = 0;
      for(begin; begin != end && inseriti< size; ++begin){  //scorro con gli iteratori passato e copio ogni elemento
	  insert(static_cast<T>(*begin));  //usando le insert inizio e fine e inUso si modificano da loro
	  inseriti ++;
	  }
	  _inUso = inseriti;
	  if(_inUso == size)
		  _fine = 0;
	  else
		  _fine = inseriti;
	}
    catch(...)   //cattura le eccezioni nel caso in cui durante il for fallisca un inserimento
      { //devo deallocare la memoria e ripristinare ad uno stato coerente
	clear(); //ripristino ad uno stato coerente
	throw;   //manda eccezzione al chiamante
      }
	  
  }


	// Solo se serve anche const_iterator aggiungere la seguente riga
	class const_iterator; // forward declaration
	
	/**
	@brief Iteratore sul cbuffer 
	Iteratore sul cbuffer	
	**/
	
	class iterator {
		T * _it;
	public:
		typedef std::random_access_iterator_tag iterator_category;
		typedef T                        value_type;
		typedef ptrdiff_t                difference_type;
		typedef T*                       pointer;
		typedef T&                       reference;

	
		iterator() : _it(0){
		}
		
		iterator(const iterator &other) : _it(0){
			_it = other._it;
		}

		iterator& operator=(const iterator &other) {
			_it = other._it;
			return * this;
		}

		~iterator() {}

		// Ritorna il dato riferito dall'iteratore (dereferenziamento)
		reference operator*() const {
			return *_it;
		}

		// Ritorna il puntatore al dato riferito dall'iteratore
		pointer operator->() const {
			return _it;
		}

		// Operatore di accesso random
		reference operator[](int index) {
			return *(_it + index);
		}
		
		// Operatore di iterazione post-incremento
		iterator operator++(int) {
			iterator tmp(*this);
			++_it;
			return tmp;
		}

		// Operatore di iterazione pre-incremento
		iterator &operator++() {
			++ _it;
			return *this;
		}

		// Operatore di iterazione post-decremento
		iterator operator--(int) {
			iterator tmp(*this);
			--_it;
			return tmp;
		}

		// Operatore di iterazione pre-decremento
		iterator &operator--() {
			--_it;
			return *this;
		}

		// Spostamentio in avanti della posizione
		iterator operator+(int offset) {
			return _it + offset;
		}

		// Spostamentio all'indietro della posizione
		iterator operator-(int offset) {
			return _it - offset;
		}
		
		// Spostamentio in avanti della posizione
		iterator& operator+=(int offset) {
			_it = _it + offset;
			return *this;
		}

		// Spostamentio all'indietro della posizione
		iterator& operator-=(int offset) {
			_it = _it - offset;
			return *this;
		}

		// Numero di elementi tra due iteratori
		difference_type operator-(const iterator &other) {
			return _it - other._it;
		}
	
		// Uguaglianza
		bool operator==(const iterator &other) const {
			return _it == other._it;
		}

		// Diversita'
		bool operator!=(const iterator &other) const {
			return _it != other._it;
		}

		// Confronto
		bool operator>(const iterator &other) const {
			return _it > other._it;
		}
		

		bool operator>=(const iterator &other) const {
			return _it >= other._it;
		}

		// Confronto
		bool operator<(const iterator &other) const {
			return _it < other._it;
		}
		
		
		// Confronto
		bool operator<=(const iterator &other) const {
			return _it <= other._it;
		}
		
		
		// Solo se serve anche const_iterator aggiungere le seguenti definizioni
		
		friend class const_iterator;

		// Uguaglianza
		bool operator==(const const_iterator &other) const {
			return _it == other._it;
		}

		// Diversita'
		bool operator!=(const const_iterator &other) const {
			return _it != other._it;
		}

		// Confronto
		bool operator>(const const_iterator &other) const {
			return _it > other._it;
		}
		

		bool operator>=(const const_iterator &other) const {
			return _it >= other._it;
		}

		// Confronto
		bool operator<(const const_iterator &other) const {
			return _it < other._it;
		}
		
		
		// Confronto
		bool operator<=(const const_iterator &other) const {
			return _it <= other._it;
		}

		// Solo se serve anche const_iterator aggiungere le precedenti definizioni
	
	private:
		//Dati membro

		// La classe container deve essere messa friend dell'iteratore per poter
		// usare il costruttore di inizializzazione.
		friend class cbuffer; // !!! Da cambiare il nome!

		// Costruttore privato di inizializzazione usato dalla classe container
		// tipicamente nei metodi begin e end
		iterator(T * indirizzo){ 
			_it = indirizzo;
		}
		
		// !!! Eventuali altri metodi privati
		
	}; // classe iterator
	
	/**
	Ritorna l'iteratore all'inizio della sequenza dati
	@return iteratore all'inizio della sequenza
	**/
	iterator begin() {
		return iterator(_buffer);
	}
	/**
	Ritorna l'iteratore alla fine della sequenza dati		
	@return iteratore alla fine della sequenza
	**/
	iterator end() {
		return iterator(_buffer + _inUso);
	}
	

	/**
	@brief Iteratore costante sul cbuffer
	Iteratore costante sul cbuffer	
	**/
	
	class const_iterator {
	const T * _it; 
	public:
		typedef std::random_access_iterator_tag iterator_category;
		typedef T                        value_type;
		typedef ptrdiff_t                difference_type;
		typedef const T*                 pointer;
		typedef const T&                 reference;

	
		const_iterator():_it(0) {
		}
		
		const_iterator(const const_iterator &other) {
			_it = other._it;
		}

		const_iterator& operator=(const const_iterator &other) {
			_it = other._it;
		}

		~const_iterator() {}

		// Ritorna il dato riferito dall'iteratore (dereferenziamento)
		reference operator*() const {
			return *_it;
		}

		// Ritorna il puntatore al dato riferito dall'iteratore
		pointer operator->() const {
			return _it;
		}

		// Operatore di accesso random
		reference operator[](int index) {
			return *(_it + index);
		}
		
		// Operatore di iterazione post-incremento
		const_iterator operator++(int) {
			const_iterator tmp(*this);
			_it ++;
			return tmp;
		}

		// Operatore di iterazione pre-incremento
		const_iterator &operator++() {
			++ _it;
			return *this;
		}

		// Operatore di iterazione post-decremento
		const_iterator operator--(int) {
			const_iterator tmp(*this);
			_it --;
			return tmp;
		}

		// Operatore di iterazione pre-decremento
		const_iterator &operator--() {
			-- _it;
			return *this;
		}

		// Spostamentio in avanti della posizione
		const_iterator operator+(int offset) {
			return _it + offset;
		}

		// Spostamentio all'indietro della posizione
		const_iterator operator-(int offset) {
			return _it - offset;
		}
		
		// Spostamentio in avanti della posizione
		const_iterator& operator+=(int offset) {
			_it = _it + offset;
			return *this;
		}

		// Spostamentio all'indietro della posizione
		const_iterator& operator-=(int offset) {
			_it = _it - offset;
			return *this;
		}

		// Numero di elementi tra due iteratori
		difference_type operator-(const const_iterator &other) {
			return _it - other._it;
		}
	
		// Uguaglianza
		bool operator==(const const_iterator &other) const {
			return _it == other._it;
		}

		// Diversita'
		bool operator!=(const const_iterator &other) const {
			return _it != other._it;
		}

		// Confronto
		bool operator>(const const_iterator &other) const {
			return _it > other._it;
		}
		

		bool operator>=(const const_iterator &other) const {
			return _it >= other._it;
		}

		// Confronto
		bool operator<(const const_iterator &other) const {
			return _it < other._it;
		}
		
		
		// Confronto
		bool operator<=(const const_iterator &other) const {
			return _it <= other._it;
		}
		
		
		// Solo se serve anche iterator aggiungere le seguenti definizioni
		
		friend class iterator;

		// Uguaglianza
		bool operator==(const iterator &other) const {
			return _it == other._it;
		}

		// Diversita'
		bool operator!=(const iterator &other) const {
			return _it != other._it;
		}

		// Confronto
		bool operator>(const iterator &other) const {
			return _it > other._it;
		}
		

		bool operator>=(const iterator &other) const {
			return _it >= other._it;
		}

		// Confronto
		bool operator<(const iterator &other) const {
			return _it < other._it;
		}
		
		
		// Confronto
		bool operator<=(const iterator &other) const {
			return _it <= other._it;
		}

		// Costruttore di conversione iterator -> const_iterator
		const_iterator(const iterator &other) {
			_it = other._it;
		}

		// Assegnamento di un iterator ad un const_iterator
		const_iterator &operator=(const iterator &other) {
			_it = other._it;
		}

		// Solo se serve anche iterator aggiungere le precedenti definizioni
	
	private:
		//Dati membro

		// La classe container deve essere messa friend dell'iteratore per poter
		// usare il costruttore di inizializzazione.
		friend class cbuffer; // !!! Da cambiare il nome!

		// Costruttore privato di inizializzazione usato dalla classe container
		// tipicamente nei metodi begin e end
		 const_iterator(const T* indirizzo) { 
			 _it = indirizzo;
		}
		
		// !!! Eventuali altri metodi privati
		
	}; // classe const_iterator
	
	/**
	Ritorna l'iteratore all'inizio della sequenza dati
	@return iteratore all'inizio della sequenza
	**/
	
	const_iterator begin() const {
		return const_iterator(_buffer);
	}
	
	/**
	Ritorna l'iteratore alla fine della sequenza dati
	@return iteratore alla fine della sequenza
	**/
	
	const_iterator end() const {
		return const_iterator(_buffer + _inUso);
	}
		
};//fine classe cbuffer


/**
Ridefinizione dell'operatore di stream per la stampa del contenuto del cbuferr
@param os oggetto stream di output
@param cb cbuffer da stampare
@return reference allo stream di output
**/

template <typename T>
std::ostream& operator<<(std::ostream &os, const cbuffer<T> & cb){
	for(typename cbuffer<T>::size_type i = 0; i < cb.inUse(); i++)
		os << cb[i] << " ";
	os << std::endl;
	return os;
}


/**
Valuta per ogni elemento del cbuffer cb dato in input, il predicato passato
@param cb cbuffer da stampare
@param P predicato da valutare su ogni elemento
**/

template<typename P, typename T>
void evaluate_if(const cbuffer<T> &cb, P predicato){
  //sfrutto gli iteratori per verificare il predicato su ogni elemento
  typename cbuffer<T>::const_iterator begin, end;
  begin = cb.begin();
  end = cb.end();
  
  //indice per la stampa
  int i = 0;
  for(begin; begin != end; begin++) {
	std::string s;
	if(predicato(*begin))	//valuto predicato su elemento
		s = "True";			//uso stringa per rendere la stampa più leggibile
	else
		s = "False";
    std::cout<<"["<<i<<"]"<<" : " << s <<std::endl; //stampo a schermo
	i++;
    }
	std::cout<<std::endl;
}

#endif
