main.exe: main.o voce.o
	g++ main.o voce.o -o main.exe

main.o: main.cpp
	g++ -DNDEBUG -c main.cpp -o main.o

voce.o: voce.cpp
	g++ -DNDEBUG -c voce.cpp -o voce.o
	
.PHONY: clean

clean:
	rm -f *.o main.exe